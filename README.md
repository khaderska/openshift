The master branch demonstrates blue-green deployment when required redundant infrastructure is available.
This implementation is to ensure that at any given time, there are 2 versions of an app/microservice is running.
The requirement is to achieve blue-green deployment for helm chart based application deployment.

Offically helmchart does not support blue-green deployment. 
Updated code is available in test branch. This implementation is not 100% successful. Hence tried a little different approach in another git project namely ""tempbluegreen"
