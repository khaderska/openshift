#!/bin/bash

version=$1

if [ $# -ne 1 ]
then
echo "Enter the version to be deployed"
echo "Usage: ./deploy.sh <image/app version>"
echo "Eg:" 
echo "./deploy.sh v3"
exit 1
fi

if [ $(oc get route routegreen > /dev/null 2>&1 ; echo $?) -eq 0 ]
then
        relname=$(oc get route routegreen -o jsonpath={.metadata.annotations."meta\.helm\.sh\/release\-name"})

        # Uninstall old release
        helm uninstall $relname

        # replace values in valuesgreen yaml
        sed -i "s/\(myappdeploy\)/\1$relname/g" helmcharts/myapp/valuesgreen.yaml
        sed -i "s/\(myappservice\)/\1$relname/g" helmcharts/myapp/valuesgreen.yaml
        sed -i "s/myapplabel/$relname/g" helmcharts/myapp/valuesgreen.yaml

        # Install new release
		echo "Installing new release($relname) for app version $version..."
        helm install --set deployment.imageversion=$version -f helmcharts/myapp/valuesgreen.yaml ${relname} ./helmcharts/myapp
else
        relname=$(oc get route routeblue -o jsonpath={.metadata.annotations."meta\.helm\.sh\/release\-name"})
        if [ $relname == release1 ]
        then
                setrel=release2
        else
                setrel=release1
        fi
        # replace values in valuesgreen yaml
        sed -i "s/\(myappdeploy\)/\1$setrel/g" helmcharts/myapp/valuesgreen.yaml
        sed -i "s/\(myappservice\)/\1$setrel/g" helmcharts/myapp/valuesgreen.yaml
        sed -i "s/myapplabel/$relname/g" helmcharts/myapp/valuesgreen.yaml
		echo "Installing new release($relname) for app version $version..."
        helm install --set deployment.imageversion=$version -f helmcharts/myapp/valuesgreen.yaml $setrel ./helmcharts/myapp
fi

echo "Enter your choice\n"
echo "yes => to proceed with deployment if the tests are successfull"
echo "no => to terminate the deployment process"
read choice

if [ $choice == "yes" ]
then
        echo "Upgrading production with new version..."

else
        echo "Cancelling deployment as test results are not satisfactory..."
        exit
fi
